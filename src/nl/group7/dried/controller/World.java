package nl.group7.dried.controller;

import java.util.*;
import java.util.stream.Collectors;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import nl.group7.dried.Main;
import nl.group7.dried.model.Cube;
import nl.group7.dried.model.CubeType;

/**
 * @author Tim Biesenbeek
 */
public class World implements Observable {

	private Collection<Cube> cubes;
	private Collection<InvalidationListener> listeners = new LinkedList<>();

	int bombs = 50;

	public World() {
		cubes = new ArrayList<>();
		for (int x = 0; x < Main.SIZE; x++) {
			for (int y = 0; y < Main.SIZE; y++) {
				for (int z = 0; z < Main.SIZE; z++) {
					if (!isBoarder(x, y, z)) {
						continue;
					}
					cubes.add(new Cube(this, x, y, z));
				}
			}
		}
	}

	private boolean isBoarder(int x, int y, int z) {
		return isBoarder(x) || isBoarder(y) || isBoarder(z);
	}

	private boolean isBoarder(int i) {
		return i == 0 || i == Main.SIZE - 1;
	}

	private boolean isInCube(int i) {
		return i >= 0 && i < Main.SIZE;
	}

	public Collection<Cube> getCubes() {
		return cubes;
	}

	public Cube getCube(int x, int y, int z) {
		return cubes.stream().filter(cube -> cube.getX() == x && cube.getY() == y && cube.getZ() == z).findFirst()
				.orElse(null);
	}

	public Cube[] getNeighbours(Cube cube) {
		return getNeighbours(cube.getX(), cube.getY(), cube.getZ());
	}

	public void revealArea(Cube start) {
		Set<Cube> area = new HashSet<>();
		area.add(start);
		area = revealArea(area, area);
		area.forEach(cube -> Arrays.stream(getNeighbours(cube)).forEach(Cube::reveal));
	}

	private Set<Cube> revealArea(Set<Cube> found, Set<Cube> newFound) {
		Set<Cube> searching = newFound;
		found.addAll(searching);
		for (Cube cube : searching) {
			newFound = Arrays.stream(getNeighbours(cube))
					.filter(neighbour -> neighbour.countBombs() == 0 && !found.contains(neighbour))
					.collect(Collectors.toSet());
			found.addAll(revealArea(found, newFound));
		}
		return found;
	}

	public Cube[] getNeighbours(int x, int y, int z) {
		Set<Cube> neighbours = new HashSet<>();
		if (isBoarder(x)) {
			for (int yOff = -1; yOff <= 1; yOff++) {
				for (int zOff = -1; zOff <= 1; zOff++) {
					int _x = x;
					int _y = y + yOff;
					int _z = z + zOff;
					if (!isInCube(_x) || !isInCube(_y) || !isInCube(_z)) {
						continue;
					}
					if (yOff == zOff && yOff == 0) {
						continue;
					}
					neighbours.add(getCube(_x, _y, _z));
				}
			}
		}
		if (isBoarder(y)) {
			for (int xOff = -1; xOff <= 1; xOff++) {
				for (int zOff = -1; zOff <= 1; zOff++) {
					int _x = x + xOff;
					int _y = y;
					int _z = z + zOff;
					if (!isInCube(_x) || !isInCube(_y) || !isInCube(_z)) {
						continue;
					}
					if (xOff == zOff && xOff == 0) {
						continue;
					}
					neighbours.add(getCube(_x, _y, _z));
				}
			}
		}
		if (isBoarder(z)) {
			for (int xOff = -1; xOff <= 1; xOff++) {
				for (int yOff = -1; yOff <= 1; yOff++) {
					int _x = x + xOff;
					int _y = y + yOff;
					int _z = z;
					if (!isInCube(_x) || !isInCube(_y) || !isInCube(_z)) {
						continue;
					}
					if (xOff == yOff && xOff == 0) {
						continue;
					}
					neighbours.add(getCube(_x, _y, _z));
				}
			}
		}
		return neighbours.toArray(new Cube[neighbours.size()]);
	}

	public void buildWorld(Cube startCube) {
		Random rand = new Random();
		Set<Cube> exclude = new HashSet<>();
		for (Cube cube : getNeighbours(startCube)) {
			exclude.add(cube);
		}
		exclude.add(startCube);
		for (int i = 0; i < bombs; i++) {
			List<Cube> remainingCubes = cubes.stream().filter(cube -> cube.getType() == null && !exclude.contains(cube))
					.collect(Collectors.toList());
			Cube random = remainingCubes.get(rand.nextInt(remainingCubes.size()));
			random.setType(CubeType.BOMB);
		}
		cubes.stream().filter(cube -> cube.getType() == null).forEach(cube -> cube.setType(CubeType.TILE));
		markDirty();
	}

	public void markDirty() {
		listeners.forEach(listener -> listener.invalidated(this));
	}

	@Override
	public void addListener(InvalidationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(InvalidationListener listener) {
		listeners.remove(listener);
	}
}
