package nl.group7.dried.controller;

import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.transform.Rotate;
import nl.group7.dried.model.Camera;
import nl.group7.dried.view.WorldView;

public class CameraController {

	private WorldView worldView;
	private double mousePosX;
	private double mousePosY;
	private double mouseOldX;
	private double mouseOldY;
	private double mouseDeltaX;
	private double mouseDeltaY;

	private Rotate rx, ry;

	public CameraController(WorldView worldView) {
		this.worldView = worldView;
		rx = new Rotate();
		rx.setAxis(Rotate.X_AXIS);
		ry = new Rotate();
		ry.setAxis(Rotate.Y_AXIS);
		worldView.getRoot().getTransforms().addAll(rx, ry);

		createAndSetCamera();
	}

	private void createAndSetCamera() {
		PerspectiveCamera camera = new PerspectiveCamera(false);
		camera.setTranslateX(-430);
		camera.setTranslateY(-280);
		camera.setTranslateZ(-500);

		Camera.getInstance().setCamera(camera);

		worldView.setCamera(Camera.getInstance().getCamera());
	}

	public void setScene(Scene scene) {
		scene.setOnMousePressed(me -> {
			mousePosX = me.getSceneX();
			mousePosY = me.getSceneY();
		});

		scene.setOnMouseDragged(me -> {
			mouseOldX = mousePosX;
			mouseOldY = mousePosY;
			mousePosX = me.getSceneX();
			mousePosY = me.getSceneY();
			mouseDeltaX = (mousePosX - mouseOldX);
			mouseDeltaY = (mousePosY - mouseOldY);
			ry.setAngle(ry.getAngle() - mouseDeltaX * Camera.getInstance().getCameraSensitivity());
			rx.setAngle(rx.getAngle() + mouseDeltaY * Camera.getInstance().getCameraSensitivity());
		});
	}

	public void setAngle(int i, boolean b) {
		if (b) {
			if (rx.getAngle() < 180) {
				ry.setAngle(ry.getAngle() - i);
				return;
			}
			ry.setAngle(ry.getAngle() + i);
			return;
		}

		if (rx.getAngle() > 180) {
			ry.setAngle(ry.getAngle() + i);
			return;
		}
		ry.setAngle(ry.getAngle() - i);
	}

	public void setAngleFront() {
		ry.setAngle(0);
		rx.setAngle(0);
	}
}
