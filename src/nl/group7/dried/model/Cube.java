package nl.group7.dried.model;

import java.util.Arrays;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;
import nl.group7.dried.Main;
import nl.group7.dried.controller.World;

/**
 * @author Tim Biesenbeek
 */
public class Cube {

	private Shape3D box;

	private World world;
	private IntegerProperty x;
	private IntegerProperty y;
	private IntegerProperty z;
	private ObjectProperty<CubeColor> color;
	private ObjectProperty<CubeType> type;
	private boolean revealed = false;

	public Cube(World w, int x, int y, int z) {
		this.world = w;
		this.x = new SimpleIntegerProperty(x);
		this.y = new SimpleIntegerProperty(y);
		this.z = new SimpleIntegerProperty(z);
		this.color = new SimpleObjectProperty<>(CubeColor.UNKNOWN);
		this.type = new SimpleObjectProperty<>(null);
	}

	public void registerBox(Shape3D box) {
		this.box = box;
		updateBoxColor();
		updatePos();
		x.addListener(change -> updatePos());
		y.addListener(change -> updatePos());
		z.addListener(change -> updatePos());
		color.addListener(change -> updateBoxColor());
		box.setOnMouseClicked(this::onClick);
	}

	private void updatePos() {
		int offset = Main.BOX_SIZE * Main.SIZE / 2;
		box.setTranslateX(getX() * Main.BOX_SIZE - offset);
		box.setTranslateY(getY() * Main.BOX_SIZE - offset);
		box.setTranslateZ(getZ() * Main.BOX_SIZE - offset);
	}

	private void updateBoxColor() {
		PhongMaterial material = new PhongMaterial();
		material.setDiffuseColor(getColor());
		box.setMaterial(material);

	}

	public void onClick(MouseEvent clicked) {
		if (getType() == null) {
			world.buildWorld(this);
		}
		switch (clicked.getButton()) {
		case PRIMARY:
			safeReveal();
			break;
		case MIDDLE:
			Arrays.stream(world.getNeighbours(this))
					.filter(cube -> cube.getCubeColor() != CubeColor.MARKED && !cube.revealed)
					.forEach(Cube::safeReveal);
			break;
		case SECONDARY:
			if (!revealed) {
				if (color.get() == CubeColor.UNKNOWN) {
					setColor(CubeColor.MARKED);
				} else if (color.get() == CubeColor.MARKED) {
					setColor(CubeColor.UNKNOWN);
				}
			}
			break;
		}
	}

	private void safeReveal() {
		if (getType() == CubeType.TILE && countBombs() == 0) {
			world.revealArea(this);
		}
		reveal();
	}

	public void reveal() {
		revealed = true;
		if (getType() == CubeType.BOMB) {
			setColor(CubeColor.BOMB);
		} else {
			reveal(countBombs());
		}
	}

	private void reveal(int bombs) {
		setColor(CubeColor.getByNumber(bombs));
	}

	public int countBombs() {
		Cube[] neighbours = world.getNeighbours(this);
		return (int) Arrays.stream(neighbours).filter(cube -> cube.getType() == CubeType.BOMB).count();
	}

	public void setType(CubeType type) {
		this.type.set(type);
	}

	public CubeType getType() {
		return type.get();
	}

	private void setColor(CubeColor color) {
		this.color.set(color);
		world.markDirty();
	}

	public CubeColor getCubeColor() {
		return color.get();
	}

	public Color getColor() {
		return color.get().getColor();
	}

	public int getX() {
		return x.get();
	}

	public int getY() {
		return y.get();
	}

	public int getZ() {
		return z.get();
	}
}
