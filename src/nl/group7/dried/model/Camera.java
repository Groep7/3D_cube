package nl.group7.dried.model;

import javafx.scene.PerspectiveCamera;

/**
 * Created by Fransman06 on 13-10-2016.
 */
public class Camera {
	private final static Camera INSTANCE = new Camera();

	private double cameraSensitivity = 2.0;
	private double scrollSpeed = 8.0;
	private PerspectiveCamera camera;

	public static Camera getInstance() {
		return INSTANCE;
	}

	private Camera() {
	}

	public double getCameraSensitivity() {
		return cameraSensitivity;
	}

	public void setCameraSensitivity(double sensitivity) {
		if (sensitivity > 0) {
			this.cameraSensitivity = sensitivity;
		}
	}

	public PerspectiveCamera getCamera() {
		return camera;
	}

	public void setCamera(PerspectiveCamera camera) {
		this.camera = camera;
	}

	public double getScrollSpeed() {
		return scrollSpeed;
	}

	public void setScrollSpeed(double scrollSpeed) {
		if (scrollSpeed > 0) {
			this.scrollSpeed = scrollSpeed;
		}

	}
}
