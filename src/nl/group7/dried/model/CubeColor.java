package nl.group7.dried.model;

import javafx.scene.paint.Color;

public enum CubeColor {

	ONE_BOMB(Color.CADETBLUE),
	TWO_BOMBS(Color.LIGHTGREEN),
	THREE_BOMBS(Color.HOTPINK),
	FOUR_BOMBS(Color.BLUE),
	FIVE_BOMBS(Color.DARKRED),
	SIX_BOMBS(Color.DARKGREEN),
	SEVEN_BOMBS(Color.PURPLE),
	EIGHT_BOMBS(Color.ORANGE),
	BOMB(Color.BLACK),
	FREE(Color.WHITESMOKE),
	UNKNOWN(Color.CYAN),
	MARKED(Color.YELLOW);

	private Color color;

	CubeColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public static CubeColor getByNumber(int x) {
		switch (x) {
		default:
			return BOMB;
		case -1:
			return UNKNOWN;
		case 0:
			return FREE;
		case 1:
			return ONE_BOMB;
		case 2:
			return TWO_BOMBS;
		case 3:
			return THREE_BOMBS;
		case 4:
			return FOUR_BOMBS;
		case 5:
			return FIVE_BOMBS;
		case 6:
			return SIX_BOMBS;
		case 7:
			return SEVEN_BOMBS;
		case 8:
			return EIGHT_BOMBS;
		}
	}
}
