package nl.group7.dried.util;

public class StringUtil {

	public static String readableEnum(Enum<?> e) {
		return readableVar(e.toString());
	}

	public static String readableVar(String javaName) {
		javaName = javaName.toString().toLowerCase().replaceAll("_", " ");
		String ret = "";
		boolean CASE = true;
		for (char c : javaName.toString().toCharArray()) {
			if (Character.isSpaceChar(c)) {
				ret += c;
				continue;
			}
			if (!Character.isLetter(c)) {
				CASE = true;
				continue;
			}
			if (CASE) {
				ret += Character.toUpperCase(c);
				CASE = false;
			} else {
				ret += Character.toLowerCase(c);
			}
		}
		return ret;
	}
}
