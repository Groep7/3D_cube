package nl.group7.dried;

import javafx.application.Application;
import nl.group7.dried.view.CameraApp;

/**
 * @author Tim Biesenbeek
 */
public class Main {
	public static final int BOX_SIZE = 60;
	public static final int SIZE = 9;

	public static void main(String[] args) {
		Application.launch(CameraApp.class, args);
	}

}
