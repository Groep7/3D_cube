package nl.group7.dried.view;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import nl.group7.dried.controller.CameraController;

public class CameraPane extends HBox {
    CameraController cameraController;

    Button turnRightButton = new Button("90° right");
    Button turnLeftButton = new Button("90° left");
    Button frontButton = new Button("Front");

    public CameraPane(CameraController cameraController) {
        super(5);
        this.cameraController = cameraController;
        setup();
        registerEvents();
    }

    private void setup() {
        getChildren().addAll(turnLeftButton, turnRightButton, frontButton);
    }

    private void registerEvents() {
        turnLeftButton.setOnAction(e -> {
            cameraController.setAngle(90, true);
        });

        turnRightButton.setOnAction(e -> {
            cameraController.setAngle(90, false);
        });

        frontButton.setOnAction(e -> {
            cameraController.setAngleFront();
        });
    }
}
