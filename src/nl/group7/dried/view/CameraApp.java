package nl.group7.dried.view;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import nl.group7.dried.controller.CameraController;

public class CameraApp extends Application {

	@Override
	public void start(Stage primaryStage) {
		WorldView worldView = new WorldView(new Group());
		CameraController cameraController = new CameraController(worldView);
		TopPane topPane = new TopPane(worldView.getWorld());
		SouthPane southPane = new SouthPane();
		LegendPane legendPane = new LegendPane();
		CameraPane cameraPane = new CameraPane(cameraController);

		VBox rightBox = new VBox();
		rightBox.getChildren().addAll(legendPane, cameraPane);

		BorderPane borderPane = new BorderPane();
		borderPane.setTop(topPane);
		borderPane.setCenter(worldView);
		borderPane.setRight(rightBox);
		borderPane.setBottom(southPane);

		Scene scene = new Scene(borderPane);

		cameraController.setScene(scene);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Minesweeper 3D");
		primaryStage.show();
	}
}
