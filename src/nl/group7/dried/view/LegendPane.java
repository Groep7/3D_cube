package nl.group7.dried.view;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import nl.group7.dried.model.CubeColor;
import nl.group7.dried.util.StringUtil;

public class LegendPane extends VBox {

	public LegendPane() {
		setup();
	}

	private void setup() {
		for (CubeColor cubeColor : CubeColor.values()) {
			HBox row = new HBox(8);
			Rectangle item = new Rectangle(16, 16);
			item.setFill(cubeColor.getColor());
			Label lbl = new Label(StringUtil.readableEnum(cubeColor));
			row.getChildren().addAll(item, lbl);
			getChildren().add(row);
		}
	}
}
