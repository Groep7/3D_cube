package nl.group7.dried.view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import nl.group7.dried.model.Camera;

public class SouthPane extends HBox {

	private Label labelC, labelS, labelSs, labelX, labelY, labelZ;

	public SouthPane() {
		setAlignment(Pos.CENTER);
		setSpacing(10);
		createLabels(Camera.getInstance());
		getChildren().addAll(labelC, labelS, labelSs, labelX, labelY, labelZ);
	}

	private void createLabels(Camera camera) {
		labelC = new Label("Camera ");
		labelS = new Label("Camera sensitivity: " + camera.getCameraSensitivity());
		labelSs = new Label("Scroll speed: " + camera.getScrollSpeed());
		labelX = new Label("X: " + camera.getCamera().getTranslateX());
		labelY = new Label("Y: " + camera.getCamera().getTranslateY());
		labelZ = new Label("Z: " + camera.getCamera().getTranslateZ());

		camera.getCamera().translateXProperty()
				.addListener(ov -> labelX.setText("X: " + camera.getCamera().getTranslateX()));

		camera.getCamera().translateYProperty()
				.addListener(ov -> labelY.setText("Y: " + camera.getCamera().getTranslateY()));

		camera.getCamera().translateZProperty()
				.addListener(ov -> labelZ.setText("Z: " + camera.getCamera().getTranslateZ()));

		labelX.setOnScroll(e -> {
			camera.getCamera().setTranslateX(
				camera.getCamera().getTranslateX() + e.getDeltaY() / camera.getScrollSpeed());
		});

		labelY.setOnScroll(e -> {
			camera.getCamera().setTranslateY(
				camera.getCamera().getTranslateY() + e.getDeltaY() / camera.getScrollSpeed());
		});

		labelZ.setOnScroll(e -> {
			camera.getCamera().setTranslateZ(
				camera.getCamera().getTranslateZ() + e.getDeltaY() / camera.getScrollSpeed());
		});

		labelS.setOnScroll(e -> {
			camera.setCameraSensitivity(camera.getCameraSensitivity() + e.getDeltaY() / 640D);
			labelS.setText("Camera sensitivity: " + camera.getCameraSensitivity());
		});

		labelSs.setOnScroll(e -> {
			camera.setScrollSpeed(camera.getScrollSpeed() + e.getDeltaY() / 40);
			labelSs.setText("Scroll speed: " + camera.getScrollSpeed());
		});
	}
}
