package nl.group7.dried.view;

import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.shape.Box;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;
import nl.group7.dried.Main;
import nl.group7.dried.controller.World;
import nl.group7.dried.model.Cube;

public class WorldView extends SubScene {

	private World world;

	public WorldView(Group root) {
		super(root, 800, 500, true, SceneAntialiasing.BALANCED);
		world = new World();
		createWorld(root);
	}

	private void createWorld(Group root) {
		boolean easteregg = Math.random() > 0.9;
		for (Cube cube : world.getCubes()) {
			if (cube == null) {
				continue;
			}
			Shape3D box;
			if (easteregg) {
				box = new Sphere(Main.BOX_SIZE);
			} else {
				box = new Box(Main.BOX_SIZE, Main.BOX_SIZE, Main.BOX_SIZE);
			}
			cube.registerBox(box);
			root.getChildren().add(box);
		}
	}

	public World getWorld() {
		return world;
	}
}
