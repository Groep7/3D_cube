package nl.group7.dried.view;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import nl.group7.dried.controller.World;
import nl.group7.dried.model.CubeColor;
import nl.group7.dried.model.CubeType;
import nl.group7.dried.util.ColoredLine;

public class TopPane extends HBox {

	public TopPane(World world) {
		setAlignment(Pos.CENTER);
		setSpacing(10);
		createLabels(world);
	}

	private void createLabels(World world) {
		String timeText = "&0Time: &2%s&0.";
		ColoredLine time = new ColoredLine(String.format(timeText, "0"));
		IntegerProperty counter = new SimpleIntegerProperty(0);
		Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), tick -> {
			counter.setValue(counter.get() + 1);
			time.setLineText(String.format(timeText, counter.get()));
		}));
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
		getChildren().add(time);

		String bombsLeftText = "&0Bombs left: &c%s&0.";
		ColoredLine bombsLeft = new ColoredLine(String.format(bombsLeftText, "None"));
		world.addListener(change -> {
			int bombs = (int) world.getCubes().stream()
					.filter(cube -> cube.getType() == CubeType.BOMB && cube.getCubeColor() != CubeColor.BOMB).count();
			int marked = (int) world.getCubes().stream().filter(cube -> cube.getCubeColor() == CubeColor.MARKED)
					.count();
			bombsLeft.setLineText(String.format(bombsLeftText, bombs - marked));
		});
		getChildren().add(bombsLeft);

		String explodedText = "&0Bombs exploded: &4%s&0.";
		ColoredLine exploded = new ColoredLine(String.format(explodedText, "None"));
		world.addListener(change -> {
			int explodedCount = (int) world.getCubes().stream().filter(cube -> cube.getCubeColor() == CubeColor.BOMB)
					.count();
			exploded.setLineText(String.format(explodedText, explodedCount));
		});
		getChildren().add(exploded);
	}
}
